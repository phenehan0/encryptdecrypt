import os

#simulates CLI behavior
def execute():
    quit = False
    while not quit:
        command = input("Enter command: ")
        os.system(command)
        q = input("quit? y|n")
        if q.lower() == "y":
            quit = True
        elif q.lower() == "n":
            pass
        else:
            print("Error: enter 'y' or 'n'")
    return
    
def exec(argv):
    for arg in argv:
        os.system(arg)

if __name__ == "__main__":
    #if wine isn't already installed'
    args = ('sudo apt-get update','sudo apt-get -y install wine',
    'wine AlKeyMaker.exe string="eeetretetretr" flag=encrypt outputfile=foo.txt',)        
    exec(args)
    #if wine is installed
    args = ('wine AlKeyMaker.exe string="string" flag=encrypt outputfile=bar.txt',)
    exec(args)
